function defaults (obj, defaultProps){
    let props = Object.keys(defaultProps);
    props.forEach(el=>{
        if(obj[el]===undefined){
            obj[el]=defaultProps[el]
        }
    })
    return obj
}



exports.defaults=defaults ;